# Photo Contest Project

An application create with Java, Spring and MariaDb that helps a team of aspiring photographers to easily manage online photo contests.
The application allows for users to register, view and participate in contests and earn points.
The organizers of the contests can add jury members, create invitational contests and invite only certain users to participate.


## Table of contents
* [How to build the project](#point1)
* [Basic functions](#point2)
* [Frontend functionalities](#point3)
* [Database relations](#point4)
* [Project images](#point5)

##Link to Swagger:

http://localhost:8080/swagger-ui/#/

##Link to Trello:
https://trello.com/b/HjGLKqzz/final-project

##How to build and run the project <a name="point1"></a>:
###Follow the link to the git repository of the project:
https://gitlab.com/vladimir-bg/photo-contest

###You can either download the project files in a format of your preference and then unpack the files in a new folder or you can clone the repository in a folder of your choosing. Please see below for more details:

####How to download project files:


![Optional Text](../photo_project/readmeImages/image1.png)


####To clone the repository, please copy the HTTPS link:

![Optional Text](../photo_project/readmeImages/image2.png)

####Go to a new folder and unpack the files if you have downloaded or clone the repository via the git clone command:

![Optional Text](../photo_project/readmeImages/image3.png)



####Once this is done you will be able to see the project files:

![Optional Text](../photo_project/readmeImages/image4.png)

###Open IntelliJ IDEA Ultimate Edition. Select Open and then find the folder containing the project files seen above. Select photo_project and click OK. The project should start building.
*NB-It will take a bit longer to build the first time.

![Optional Text](../photo_project/readmeImages/image5.png)


##How to create and fill the database with data:

###On the right of your screen you should see the database tab. Click on it and select the + sign.
From there select Data Source and from the drop down list select MariaDB.

![Optional Text](../photo_project/readmeImages/image6.png)


####Login with your credentials and start the database (you may have to configure the dialect):

![Optional Text](../photo_project/readmeImages/image7.png)


####Open the project tab and find the database directory.
Open photo_contest_db.sql. This script will create the scheme in the database and all its tables and relations.

![Optional Text](../photo_project/readmeImages/image8.png)


####You can execute the scrip in several ways. For example, select all lines, right click and Execute via the Console.

Afterwards you should be able to see the created scheme and tables in the database:

![Optional Text](../photo_project/readmeImages/image9.png)


####After this is done you must select the script to fill in the database tables with information: 	insert-data.sql. You may execute it in the same way as the last script.

####If the folder /photo_project/src/main/resources/static/userPhotos
is empty you may select all the photos from the database/insert-db starting photos and paste them in the resources/static/userPhotos.




####Go to /photo_project/src/main/resources and update the database.username and database.password to match yours
You must also update the file.upload-dir based on where you have saved the project.

##Usage <a name="point2"></a>
Before starting the application please make sure that you go to resources→application.properties and make sure the scheduling.enabled=true
This makes sure that the application checks the contest phases, marks them as finished when the phase 2 time is up, compares the entry reviews and distributes points to the contestants as per the rules.

![Optional Text](../photo_project/readmeImages/image11.png)


The application has Rest controllers which allow for the the main features to be used with developer tools such as Postman.


![Optional Text](../photo_project/readmeImages/image12.png)


###The contest controller allows for organizers to create contests, add jury to contests, add users to invitational contests, review all users and all contests in all phases.

###The entry controller allows for users to create contest entries and add photos to them.

###The photo controller allows for users and organizers to upload photos as multipart files, upload photos from URL-s, view all saved photos

###The photo junkie controller allows for the users to view contest in which they can participate, view their entries and the jury reviews. In addition users can add photos to their archives. It also allows for users to register, update and deactivate their accounts.

###The review controller allows for contests jury members to view   the entries they must review, add score and rank or mark the entry as not aligning with the contest category.

##Front-end feature usage <a name="point3"></a>
To use the application features more easily you must start the server and make sure that everything loads correctly.
For example:

![Optional Text](../photo_project/readmeImages/image13.png)


Next, open a browser and got to http://localhost:8080/

The frontpage should load where you can see information about the last contest winning photos as well as buttons for new users to register or to log in.

![Optional Text](../photo_project/readmeImages/image14.png)


###When you log in as an organizers the application redirects you to the organizers’ dashboard.
Visible on the page are currently active contests (in phase 1 or 2).

![Optional Text](../photo_project/readmeImages/image15.png)


The buttons allow for:

####Contests in phase 1: allows organizers to review such contests and see the submitted entries.
####Contests in phase 2: allows organizers to see contests in this phase and since all organizers are by default jury members, they can also add reviews to entries.

![Optional Text](../photo_project/readmeImages/image16.png)


![Optional Text](../photo_project/readmeImages/image17.png)


The Wrong category button allows for automatic review creation with 0 points and message that the entry and category do not match.

![Optional Text](../photo_project/readmeImages/image18.png)


###The Finished Contests button is self explanatory.

###The Junkies button allows organizers to see the users with details about their rank and current score.

![Optional Text](../photo_project/readmeImages/image19.png)


###The create contest button allows for new contests to be created. The organizer must add a unique title, set phase 1 (# of  days and hours in the future, respectively).
If the Close checkbox is selected the contest will be invitational only the the organizer must add users to it (automatically redirected to that option).
The organizer can upload a cover photo (only .jpeg, .png and .jpg formats accepted) using a file, a URL link or from the his current archive.
![Optional Text](../photo_project/readmeImages/image20.png)

###When logged in as a regular user the application redirects you to the users dashboard.

![Optional Text](../photo_project/readmeImages/image21.png)
Here the users can:
###Edit or deactivate their profile via the Account Edit button.

###View Rank and Score does just that:

![Optional Text](../photo_project/readmeImages/image22.png)


###Upload photo allows for users to add photos to their archive and later use them in contests.

###View My Photos allows the users to see their photo archive as well as the photos with which they have entered in contests.

###Entered Finished Contests allows for users to see their all entries for the contest in which they have participated as well as all jury rates and comments.
###Entered Active contests shows only the contests in P1 and P2 and the user’s own entries.

###View Available to Enter Open/Invitational contests allows for users to see such contests and create entries:

![Optional Text](../photo_project/readmeImages/image23.png)


###If the user is with a Master rank or greater, he/she will be able to check if he has been invited as jury and add reviews for other user’s entries (same functionality and view as for the organizers).


#Database relations <a name="point4"></a>:

![Optional Text](../photo_project/readmeImages/image10.png)

##Images of the project <a name="point5"></a>:


![Optional Text](../photo_project/readmeImages/image24.png)

![Optional Text](../photo_project/readmeImages/image25.png)

![Optional Text](../photo_project/readmeImages/image26.png)

![Optional Text](../photo_project/readmeImages/image27.png)

![Optional Text](../photo_project/readmeImages/image28.png)

![Optional Text](../photo_project/readmeImages/image29.png)



