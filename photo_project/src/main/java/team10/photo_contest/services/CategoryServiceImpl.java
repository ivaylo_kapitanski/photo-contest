package team10.photo_contest.services;

import org.springframework.stereotype.Service;
import team10.photo_contest.models.Category;
import team10.photo_contest.repositories.contracts.CategoryRepository;
import team10.photo_contest.services.contracts.CategoryService;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByCategory(String category) {
        return categoryRepository.getByCategory(category);
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }
}
