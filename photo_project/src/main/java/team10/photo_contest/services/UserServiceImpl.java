package team10.photo_contest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.services.contracts.UserService;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.DUPLICATE_USER;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final VerificationHelper verificationHelper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           VerificationHelper verificationHelper) {
        this.userRepository = userRepository;
        this.verificationHelper = verificationHelper;
    }

    @Override
    public User create(User user) {
        validateUnique(user);
        userRepository.create(user);
        return user;
    }

    @Override
    public void update(User userToUpdate, User user) {
        try{
            userRepository.getByUsername(userToUpdate.getUserName());
        }catch (EntityNotFoundException e){
            throw new EntityNotFoundException(e.getMessage());
        }
        validateUnique(userToUpdate);
        verificationHelper.verifyUserCanView(userToUpdate.getUserId(), user);
        userRepository.update(userToUpdate);
    }

    @Override
    public void deactivate(int id, User user) {
        verificationHelper.verifyUserCanView(id,user);
        userRepository.deactivate(id);
    }

    @Override
    public User getById(int id, User user) {
        verificationHelper.checkIfOrganizer(user);
        return userRepository.getById(id);
    }

    @Override
    public List<User> getAllAndSort(User user) {
        verificationHelper.checkIfOrganizer(user);
        return userRepository.getAllAndSort();
    }
    @Override
    public int getTotalUsers() {

        return userRepository.getAllAndSort().size();
    }

    @Override
    public User getByUsername(String username) {
         return userRepository.getByUsername(username);
    }

    private void validateUnique(User user) {
        boolean duplicateExists = true;
        try {
            User existing = userRepository.getByUsername(user.getUserName());
            if (existing.getUserId() == user.getUserId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(DUPLICATE_USER);
        }
    }
}
