package team10.photo_contest.services.contracts;

import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

import java.util.List;

public interface EntryService {
    Entry getById(int id);
    List<Entry> getAll();
    List<Entry> getByUserId(int id);
    void create(Entry entry);
    void update(Entry entry, User user);
    EntryViewDto viewMyContestEntry(User user, int id);
    int viewFinishedContestEntryScore(int entryId);
    List<Review> viewMyContestEntryReviews(int id,User user);
}
