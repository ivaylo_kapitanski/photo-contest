package team10.photo_contest.services.contracts;

import team10.photo_contest.models.Category;

import java.util.List;

public interface CategoryService {
    Category getById(int id);
    Category getByCategory(String category);
    List<Category> getAll();
}
