package team10.photo_contest.services.contracts;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.controllers.utils.FileModel;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.models.Photo;
import team10.photo_contest.models.User;

import java.util.List;

public interface PhotoService {
    void update(Photo photo);
    void delete(int id);
    Photo getById(int id);
    List<Photo> getByUserId(int userId);
    Boolean verifyIfContestPhoto(int id);
    Resource viewPhoto(int fileName, User user);
    Photo storeFile(MultipartFile file, User user);
    Photo storeFromUrl(PhotoFromUrlDto dto, User user);
    Photo saveOrGetContestPhoto(FileModel file, EntryDto dto, User user);
}
