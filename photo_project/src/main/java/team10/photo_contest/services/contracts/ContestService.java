package team10.photo_contest.services.contracts;

import team10.photo_contest.dtos.output.EntryWithReviewsDto;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

import java.util.List;

public interface ContestService {
    Contest getById(int id);
    List<Contest> getAll(User user);
    List<Contest> viewInPhase1(User user);
    List<Contest> viewInPhase2(User user);
    List<Contest> viewFinished(User user);
    List<Contest> viewOpen(User user);
    List<Contest> viewOpenInPhase1();
    List<Contest> finishedContestForUser(int id);
    List<Contest> activeContestForUser(int id);
    List<Contest> viewInvitationalInPhase1(User user);
    List<Review> viewFinishedContestEntryReviews(User user, int userId);
    List<EntryWithReviewsDto> viewAllFinishedContestEntryReviews(User user, int contestId);
    List<User> viewJunkies(int id, User user);
    List<User> viewJury(int id, User user);
    void create(Contest contest, User user);
    Contest updateJury(int id, List<String> juryToAdd, User user);
    Contest updateJunkies(int id, List<String> junkiesToAdd, User user);
    boolean isContestPhase2(int id);
    void update(Contest contest);
    List<Contest> contestsNotEnteredByUser(List<Contest> contests, User user);
    List<Entry> getEntriesByContestId(int id);
    List<Contest> viewContestsWhenJury(User user);
    List<Entry> viewLastThreeWinningEntries();
}
