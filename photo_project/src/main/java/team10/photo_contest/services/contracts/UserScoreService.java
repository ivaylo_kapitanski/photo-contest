package team10.photo_contest.services.contracts;

import team10.photo_contest.dtos.output.RankDto;
import team10.photo_contest.models.User;
import team10.photo_contest.models.UserScore;

import java.util.List;


public interface UserScoreService {
    UserScore getById(int id);
    List<User> getByRank(int min, int max);
    void update(User user, UserScore rankAndScore);
    UserScore create(User user, UserScore rankAndScore);
    RankDto getByUserId(int id, User user);
}
