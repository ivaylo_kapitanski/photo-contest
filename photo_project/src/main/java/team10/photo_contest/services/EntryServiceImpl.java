package team10.photo_contest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.mapers.EntryModelMapper;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.services.contracts.EntryService;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.Date;
import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.*;

@Service
public class EntryServiceImpl implements EntryService {
    private final EntryRepository entryRepository;
    private final VerificationHelper verificationHelper;
    private final EntryModelMapper entryModelMapper;

    @Autowired
    public EntryServiceImpl(EntryRepository entryRepository, VerificationHelper verificationHelper, EntryModelMapper entryModelMapper) {
        this.entryRepository = entryRepository;
        this.verificationHelper = verificationHelper;
        this.entryModelMapper = entryModelMapper;
    }

    @Override
    public Entry getById(int id) {
        return entryRepository.getById(id);
    }

    @Override
    public List<Entry> getAll() {
        return entryRepository.getAll();
    }

    @Override
    public List<Entry> getByUserId(int id) {
        return entryRepository.getByUserId(id);
    }

    @Override
    public void create(Entry entry) {
        User user = entry.getUser();
        Contest contest = entry.getContest();
        if (contest.getContestEntries().stream().anyMatch(s -> s.getUser().getUserId() == user.getUserId()))
            throw new DuplicateEntityException(USER_CAN_HAVE_ONE_ENTRY);

        if (!contest.isOpen() && contest.getInvitedJunkies().stream().noneMatch(s -> s.getUserName().equals(user.getUserName())))
            throw new EntityNotFoundException(USER_NOT_INVITED);
        if (contest.getJury().stream().anyMatch(s -> s.getUserName().equals(user.getUserName())))
            throw new NotAllowedOperationException(USER_IS_JURY);
        if (contest.getPhaseITime().after(new Date())) {
            entryRepository.create(entry);
        } else throw new NotAllowedOperationException(USER_ENTRY_NOT_ALLOWED);
    }

    @Override
    public void update(Entry entry, User user) {
        verificationHelper.verifyUserCanModifyEntry(entry, user);
        if (entry.getPhoto() != null) {
            throw new NotAllowedOperationException(PHOTO_EDIT_ERROR);
        }
        entryRepository.update(entry);
    }

    @Override
    public EntryViewDto viewMyContestEntry(User user, int id) {
        Entry entry = entryRepository.viewMyContestEntry(user.getUserId(), id);
        return entryModelMapper.toDto(entry,new EntryViewDto());
    }

    @Override
    public int viewFinishedContestEntryScore(int entryId) {
        Entry entry = entryRepository.getById(entryId);
        int totalScore = entry.getReviews().stream().mapToInt(Review::getScore).sum();
        if(entry.getContest().getJury().size()==0){
            return 0;
        }
        return totalScore / entry.getContest().getJury().size();
    }

    @Override
    public List<Review> viewMyContestEntryReviews(int id, User user) {
        if(entryRepository.getById(id).getContest().getContestEntries()
                .stream().anyMatch(e->e.getUser().getUserId()==(user.getUserId()))||user.isOrganizer()) {
            return entryRepository.viewMyContestEntryReviews(id);
        }throw new NotAllowedOperationException(ENTRY_VIEW_ERROR);
    }
}
