package team10.photo_contest.services.utils;

import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

public interface VerificationHelper {

    void verifyJuryCanModifyReview(Review review, User user);

    void verifyUserCanView(int userToModifyId, User user);

    void checkIfOrganizer(User user);

    Contest checkIfContestExists(int contestId);

    void verifyUserCanModifyEntry(Entry entry, User user);

}
