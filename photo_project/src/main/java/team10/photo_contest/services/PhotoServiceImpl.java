package team10.photo_contest.services;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.controllers.utils.FileModel;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.exceptions.*;
import team10.photo_contest.models.Photo;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.PhotoRepository;
import team10.photo_contest.services.contracts.PhotoService;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

import static team10.photo_contest.exceptions.ErrorMessages.*;

@Service
public class PhotoServiceImpl implements PhotoService {
    private final PhotoRepository photoRepository;
    private Path fileStorageLocation;


    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository,
                            Path fileStorageLocation) {
        this.photoRepository = photoRepository;
        this.fileStorageLocation = fileStorageLocation;

    }

    @PostConstruct
    public void initFileStorageLocation() {
        try {
            Files.createDirectories(this.fileStorageLocation);

        } catch (Exception e) {
            throw new FileStorageException
                    ("Could not create the directory where the uploaded files will be stored.", e);
        }
    }

    @Override
    public Photo storeFromUrl(PhotoFromUrlDto dto, User user) {
        String fileName = getUniqueFileName(dto.getPhotoUrl());

        if (fileName.contains("..")) {
            throw new FileStorageException(VALID_PHOTO_NAME);
        }
        if (photoRepository.checkIfPhotoNameIsUsed(user.getUserId(), dto.getPhotoName())) {
            throw new DuplicateEntityException(PHOTO_DUPLICATE);
        }
        Path targetLocation = fileStorageLocation.resolve(fileName);
        int CONNECT_TIMEOUT = 10000;
        int READ_TIMEOUT = 10000;
        try {
            FileUtils.copyURLToFile(new URL(dto.getPhotoUrl()),
                    new File(targetLocation.toAbsolutePath().toString()), CONNECT_TIMEOUT, READ_TIMEOUT);

            Photo photoToSave = new Photo();
            photoToSave.setUserId(user.getUserId());
            photoToSave.setSource(fileName);
            photoToSave.setPhotoName(dto.getPhotoName());
            photoRepository.create(photoToSave);
            return photoToSave;

        } catch (IOException ex) {
            throw new FileStorageException(PHOTO_SAVE_EXCEPTION, ex);
        }
    }

    @Override
    public Photo storeFile(MultipartFile file, User user) {
        if (photoRepository.checkIfPhotoNameIsUsed(user.getUserId(), file.getOriginalFilename())) {
            throw new DuplicateEntityException(PHOTO_DUPLICATE);
        }
        if (file.isEmpty()) {
            throw new FileStorageException(PHOTO_MISSING);
        }

        String fileName = StringUtils.cleanPath(getUniqueFileName(file));
        if (fileName.contains("..")) {
            throw new FileStorageException(PHOTO_SAVE_EXCEPTION);
        }
        try {
            Path targetLocation = fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            Photo photoToSave = new Photo();
            photoToSave.setUserId(user.getUserId());
            photoToSave.setSource(fileName);
            photoToSave.setPhotoName(file.getOriginalFilename());
            photoRepository.create(photoToSave);
            return photoToSave;
        } catch (IOException ex) {
            throw new FileStorageException(PHOTO_SAVE_EXCEPTION, ex);
        }
    }

    @Override
    public Resource viewPhoto(int photoId, User user) {
        Photo photoToDisplay;
        try {
            photoToDisplay = photoRepository.getById(photoId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(String.format(PHOTO_NOT_FOUND, photoId));
        }
        if (photoToDisplay.getUserId() != user.getUserId()) {
            throw new UnauthorizedOperationException("Users can view only their own photos!");
        }

        try {
            Path filePath = fileStorageLocation.resolve(photoToDisplay.getSource()).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException(String.format(PHOTO_NOT_FOUND, photoId));
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException(String.format(PHOTO_NOT_FOUND, photoId), ex);
        }
    }

    @Override
    public void update(Photo photo) {
        photoRepository.update(photo);
    }

    @Override
    public void delete(int id) {
        if (!verifyIfContestPhoto(id)) {
            photoRepository.delete(id);
        } else throw new UsedPhotoException("Cover or contest photo");
    }

    @Override
    public Photo saveOrGetContestPhoto(FileModel file, EntryDto dto, User user) {
        Photo photo;

        if (!file.getFile().isEmpty()) {
            MultipartFile newFile = file.getFile();
            photo = storeFile(newFile, user);

        } else if (!dto.getPhotoUrl().isEmpty()) {
            PhotoFromUrlDto newDto = new PhotoFromUrlDto();
            newDto.setPhotoName(dto.getPhotoName());
            newDto.setPhotoUrl(dto.getPhotoUrl());
            photo = storeFromUrl(newDto, user);
        } else if (dto.getContestId() != 0) {
            photo = getById(dto.getPhotoIdFromUploaded());
        } else {
            throw new NotAllowedOperationException("Please select a valid option");
        }
        return photo;
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public List<Photo> getByUserId(int userId) {
        return photoRepository.getByUserId(userId);
    }

    @Override
    public Boolean verifyIfContestPhoto(int id) {
        return photoRepository.verifyIfContestPhoto(id);
    }

    private String getUniqueFileName(MultipartFile file) {
        String filename = file.getOriginalFilename();
        checkFileExtension(filename);
        return String.format("%s-%s",
                UUID.randomUUID(),
                file.getOriginalFilename());
    }

    private String getUniqueFileName(String photoUrl) {
        String fileName;
        try {
            fileName = Paths.get(new URI(photoUrl).getPath()).getFileName().toString();

        } catch (URISyntaxException e) {
            throw new FileStorageException(PHOTO_SAVE_EXCEPTION);
        }
        checkFileExtension(fileName);
        return String.format("%s-%s",
                UUID.randomUUID(),
                fileName);
    }

    private void checkFileExtension(String fileName) {
        String fileExtension = FilenameUtils.getExtension(fileName).toLowerCase();
        if (!fileExtension.equals("jpeg") && !fileExtension.equals("png") && !fileExtension.equals("jpg")) {
            throw new FileStorageException(EXTENSION_NOT_CORRECT);
        }

    }
}


