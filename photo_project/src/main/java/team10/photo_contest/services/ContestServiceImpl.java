package team10.photo_contest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.dtos.output.EntryWithReviewsDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.exceptions.ContestIsNotInvitationalException;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.EntryService;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.*;
import java.util.stream.Collectors;

import static team10.photo_contest.exceptions.ErrorMessages.*;

@Service
public class ContestServiceImpl implements ContestService {
    public static final String APPEND_DELIMITER = ", ";
    private final ContestRepository contestRepository;
    private final VerificationHelper verificationHelper;
    private final UserRepository userRepository;
    private final EntryService entryService;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository,
                              VerificationHelper verificationHelper,
                              UserRepository userRepository,
                              EntryService entryService) {
        this.contestRepository = contestRepository;
        this.verificationHelper = verificationHelper;
        this.userRepository = userRepository;
        this.entryService = entryService;
    }

    @Override
    public List<Contest> getAll(User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.getAll();
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public void create(Contest contest, User user) {
        verificationHelper.checkIfOrganizer(user);
        validateUnique(contest);
        Set<User> organizerSet = new HashSet<>(userRepository.getAllOrganizers());
        contest.setJury(organizerSet);
        contestRepository.create(contest);
    }

    @Override
    public List<Contest> viewInPhase1(User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewInPhase1();
    }

    @Override
    public List<Contest> viewInPhase2(User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewInPhase2();
    }

    @Override
    public List<Contest> viewContestsWhenJury(User user) {
        List<Contest> getContests = contestRepository.getAll();
        List<Contest> finalList = new ArrayList<>();
        for (Contest contest : getContests) {
            if (contestRepository.isJurorInContest(contest.getContestId(), user.getUserId())) {
                finalList.add(contest);
            }
        }
        if (finalList.size() == 0) {
            throw new EntityNotFoundException(NO_CONTESTS_THIS_PHASE);
        }
        return finalList;
    }

    @Override
    public List<Contest> viewFinished(User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewFinished();
    }

    @Override
    public List<Contest> viewOpen(User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewOpen();
    }

    @Override
    public List<Contest> viewInvitationalInPhase1(User user) {
        return contestRepository.viewInvitationalInPhase1(user);
    }

    @Override
    public List<Review> viewFinishedContestEntryReviews(User user, int contestId) {
        Contest contest = contestRepository.getById(contestId);
        if (contest.getContestEntries().stream().noneMatch(e -> e.getUser().getUserId() == user.getUserId())) {
            throw new EntityNotFoundException(NO_ENTRIES_FROM_USER );
        }
        return contestRepository.viewFinishedContestEntryReviews(user.getUserId(), contestId);
    }

    @Override
    public List<EntryWithReviewsDto> viewAllFinishedContestEntryReviews(User user, int contestId) {
        Contest contest = contestRepository.getById(contestId);
        if (contest.getContestEntries().stream().noneMatch(e -> e.getUser().getUserId() == user.getUserId())) {
            throw new EntityNotFoundException(NO_ENTRIES_FROM_USER );
        }
        List<Entry> entriesToMap = contestRepository.viewAllFinishedContestEntryReviews(contestId);
        List<EntryWithReviewsDto> listToShow = new ArrayList<>();
        for (Entry entry : entriesToMap) {
            EntryWithReviewsDto viewEntryDto = new EntryWithReviewsDto();
            mapEntryToDto(entry, viewEntryDto);
            listToShow.add(viewEntryDto);
        }
        return listToShow;
    }

    @Override
    public List<Contest> viewOpenInPhase1() {
        return contestRepository.viewOpenInPhase1();
    }

    @Override
    public List<Contest> finishedContestForUser(int id) {
        return contestRepository.finishedContestForUser(id);
    }

    @Override
    public List<Contest> activeContestForUser(int id) {
        return contestRepository.activeContestForUser(id);
    }

    @Override
    public List<User> viewJunkies(int id, User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewJunkies(id);
    }

    @Override
    public List<Contest> contestsNotEnteredByUser(List<Contest> contests, User user) {
        return contests
                .stream()
                .filter(c -> c.getContestEntries().stream().noneMatch(e -> e.getUser().getUserId() == user.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Entry> getEntriesByContestId(int id) {
        return contestRepository.getEntriesByContestId(id);
    }

    @Override
    public List<User> viewJury(int id, User user) {
        verificationHelper.checkIfOrganizer(user);
        return contestRepository.viewJury(id);
    }

    @Override
    public Contest updateJury(int id, List<String> juryToAdd, User user) {
        verificationHelper.checkIfOrganizer(user);
        Contest contest = verificationHelper.checkIfContestExists(id);
        StringBuilder usersNotFound = new StringBuilder();
        StringBuilder usersNotMaster = new StringBuilder();
        StringBuilder alreadyAdded = new StringBuilder();

        for (String userName : juryToAdd) {
            User userToAdd;
            try {
                userToAdd = userRepository.getByUsername(userName);
            } catch (EntityNotFoundException e) {
                usersNotFound.append(userName);
                usersNotFound.append(APPEND_DELIMITER);
                continue;
            }
            if (!userRepository.isUserMaster(userName)) {
                usersNotMaster.append(userName);
                usersNotMaster.append(APPEND_DELIMITER);
                continue;
            }
            if (contestRepository.isJurorInContest(id, userToAdd.getUserId())) {
                alreadyAdded.append(userName);
                alreadyAdded.append(APPEND_DELIMITER);
                continue;
            }
            contest.getJury().add(userRepository.getByUsername(userName));
        }

        contestRepository.update(contest);
        if (!usersNotFound.toString().isEmpty() || !usersNotMaster.toString().isEmpty()
                || !alreadyAdded.toString().isEmpty()) {
            throw new NotAllowedOperationException(getCombinedError(usersNotFound, usersNotMaster, alreadyAdded));
        }
        return contest;
    }

    @Override
    public Contest updateJunkies(int id, List<String> junkiesToAdd, User user) {
        verificationHelper.checkIfOrganizer(user);
        Contest contest = verificationHelper.checkIfContestExists(id);
        if (contest.isOpen()) {
            throw new ContestIsNotInvitationalException(CONTEST_IS_OPEN);
        }
        StringBuilder usersNotFound = new StringBuilder();
        StringBuilder userIsJury = new StringBuilder();
        StringBuilder alreadyAdded = new StringBuilder();

        for (String userName : junkiesToAdd) {
            User userToAdd;
            try {
                userToAdd = userRepository.getByUsername(userName);
            } catch (EntityNotFoundException e) {
                usersNotFound.append(userName);
                usersNotFound.append(APPEND_DELIMITER);
                continue;
            }
            if (contestRepository.isJurorInContest(id, userToAdd.getUserId())) {
                userIsJury.append(userName);
                userIsJury.append(APPEND_DELIMITER);
                continue;
            }
            if (contestRepository.isJunkieInContest(id, userToAdd.getUserId())) {
                alreadyAdded.append(userName);
                alreadyAdded.append(APPEND_DELIMITER);
                continue;
            }
            contest.getInvitedJunkies().add(userRepository.getByUsername(userName));
        }
        contestRepository.update(contest);
        if (!usersNotFound.toString().isEmpty() || !userIsJury.toString().isEmpty()
                || !alreadyAdded.toString().isEmpty()) {
            throw new NotAllowedOperationException(getCombinedError(usersNotFound, userIsJury, alreadyAdded));
        }
        return contest;
    }

    @Override
    public boolean isContestPhase2(int id) {
        Contest contest = getById(id);
        Date date = new Date();
        return contest.getPhaseITime().before(date) && contest.getPhaseIITime().after(date);
    }

    @Override
    public void update(Contest contest) {
        contestRepository.update(contest);
    }

    private void validateUnique(Contest contest) {
        boolean duplicateExists = true;
        try {
            Contest existingContest = contestRepository.getByTitle(contest.getTitle());
            if (existingContest.getContestId() == contest.getContestId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException(CONTEST_TITLE_EXISTS);
        }
    }

    public List<Entry> viewLastThreeWinningEntries() {
        List<Contest> lastThree = contestRepository.viewFinished().stream()
                .sorted(Comparator.comparing(Contest::getPhaseIITime))
                .limit(3).collect(Collectors.toList());

        List<Entry> winningEntries = new ArrayList<>();
        for (Contest contest : lastThree) {
            winningEntries.add(getBestEntry(contest));
        }
        if (winningEntries.size() == 0) {
            throw new EntityNotFoundException(NO_WINNING_ENTRIES);
        }
        return winningEntries;
    }

    private Entry getBestEntry(Contest contest) {
        Entry entry = new Entry();
        int maxScore = 0;
        for (Entry contestEntry : contest.getContestEntries()) {

            if (entryService.viewFinishedContestEntryScore(contestEntry.getEntryId()) >= maxScore) {
                entry = contestEntry;
                maxScore = entryService.viewFinishedContestEntryScore(contestEntry.getEntryId());
            }
        }
        return entry;
    }

    private String getCombinedError(StringBuilder inputOne, StringBuilder inputTwo,
                                    StringBuilder inputThree) {
        StringBuilder combinedError = new StringBuilder();
        combinedError.append("Users not found: ");
        combinedError.append(inputOne.substring(0, inputOne.length()));
        combinedError.append("/ ");
        combinedError.append("Users is part of :");
        combinedError.append(inputTwo.substring(0, inputTwo.length()));
        combinedError.append("/ ");
        combinedError.append("Users already added: ");
        combinedError.append(inputThree.substring(0, inputThree.length()));
        return combinedError.toString();
    }

    private void mapEntryToDto(Entry entry, EntryWithReviewsDto dto) {
        dto.setContestId(entry.getContest().getContestId());
        dto.setEntryId(entry.getEntryId());
        dto.setEntryStory(entry.getStory());
        dto.setEntryTitle(entry.getTitle());
        dto.setCreatedBy(entry.getUser().getUserId());
        dto.setPhotoId(entry.getPhoto().getPhotoId());
        dto.setPhotoSoruce(entry.getPhoto().getSource());

        List<ReviewShowDto> tempList = new ArrayList<>();
        for (Review review : entry.getReviews()) {
            tempList.add(new ReviewShowDto(review.getComment(),
                    review.getScore(),
                    review.getReviewId(),
                    review.getCreatedBy(),
                    review.getContestEntry().getEntryId()));
        }
        dto.setReviewShowDto(tempList);
    }
}
