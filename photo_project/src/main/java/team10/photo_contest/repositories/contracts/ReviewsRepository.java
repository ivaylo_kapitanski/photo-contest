package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

import java.util.List;

public interface ReviewsRepository {
    Review getById(int id);
    List<Review> getAll();
    List<Review> getByScore(int score);

    List<Review> getByEntry(int entryId);
    Review create(Review review);
    Review update(Review review);

    Review getByEntryAndJuror(int entryId,int userId);
    List<Entry> viewEntriesForInvitedContests(User user);
}
