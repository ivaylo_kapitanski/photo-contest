package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;

import java.util.List;

public interface EntryRepository {
    Entry getById(int id);

    List<Entry> getAll();

    List<Entry> getByUserId(int id);

    void create(Entry entry);

    void update(Entry entry);

    Entry viewMyContestEntry(int userId, int id);
    List<Review> viewMyContestEntryReviews(int id);
}
