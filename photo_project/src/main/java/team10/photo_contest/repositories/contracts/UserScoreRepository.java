package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.User;
import team10.photo_contest.models.UserScore;

import java.util.List;

public interface UserScoreRepository {
    List<User> getByRank(int min, int max);
    void create(UserScore rankAndScore);
    void update(UserScore rankAndScore);
    UserScore getById(int id);
    UserScore getByUserId(int userId);
}
