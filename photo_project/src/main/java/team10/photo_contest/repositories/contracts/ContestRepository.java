package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

import java.util.List;

public interface ContestRepository {
    Contest getById(int id);

    List<Contest> getAll();

    List<Contest> viewInPhase1();

    List<Contest> viewInPhase2();

    List<Contest> viewFinished();

    List<Contest> viewOpen();

    List<Contest> viewOpenInPhase1();

    List<Contest> finishedContestForUser(int id);

    List<Contest> activeContestForUser(int id);

    List<Contest> viewInvitationalInPhase1(User user);

    List<Entry> viewAllFinishedContestEntryReviews(int contestId);

    List<Review> viewFinishedContestEntryReviews(int userId, int contestId);

    List<User> viewJunkies(int id);

    List<User> viewJury(int id);

    void create(Contest contest);

    void update(int id);

    void update(Contest contest);

    boolean isJurorInContest(int contestId, int userId);

    boolean isJunkieInContest(int contestId, int userId);

    List<Entry> getEntriesByContestId(int id);

    Contest getByTitle(String title);


}
