package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Photo;

import java.util.List;

public interface PhotoRepository{

    void create(Photo photo);

    void update(Photo photo);

    void delete(int id);

    Photo getById(int id);

    List<Photo> getByUserId(int userId);

    Boolean verifyIfContestPhoto(int id);

    boolean checkIfPhotoNameIsUsed(int userId,String name);
}
