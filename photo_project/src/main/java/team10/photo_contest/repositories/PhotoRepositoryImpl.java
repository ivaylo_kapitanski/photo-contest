package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Photo;
import team10.photo_contest.repositories.contracts.PhotoRepository;

import java.util.List;

@Repository
public class PhotoRepositoryImpl implements PhotoRepository {


    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.save(photo);
        }
    }

    @Override
    public void update(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(photo);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Photo photoToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(photoToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public Photo getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Photo photo = session.get(Photo.class, id);
            if (photo == null) {
                throw new EntityNotFoundException("Photo", id);
            }
            return photo;
        }
    }

    @Override
    public List<Photo> getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Photo> query = session.createQuery(
                    "from Photo where userId=:userId", Photo.class);
            query.setParameter("userId", userId);

            List<Photo> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("Photos for user with id: %d not found", userId));
            }
            return result;
        }
    }

    @Override
    public boolean checkIfPhotoNameIsUsed(int userId,String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Photo> query = session.createQuery(
                    "from Photo where userId=:userId and photoName=:name", Photo.class);
            query.setParameter("userId", userId);
            query.setParameter("name", name);

            List<Photo> result = query.list();
            return !result.isEmpty();
        }
    }


    @Override
    public Boolean verifyIfContestPhoto(int id) {
        try (Session session = sessionFactory.openSession()) {
            Photo photo = session.get(Photo.class, id);
            return photo.isContestPhoto();
        }
    }
}
