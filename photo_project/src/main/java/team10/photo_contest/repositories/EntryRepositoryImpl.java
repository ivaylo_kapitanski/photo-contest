package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.EntryRepository;

import java.util.List;

@Repository
public class EntryRepositoryImpl implements EntryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public EntryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Entry> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(" from Entry ", Entry.class);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Entry");
            return query.list();
        }
    }

    @Override
    public Entry getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(" select e from Entry e where e.id = :id ", Entry.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Entry", id);
            return query.list().get(0);
        }
    }

    @Override
    public List<Entry> getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(" select e from Entry e left join e.user u where u.id = :id ", Entry.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Entry", id);
            return query.list();
        }
    }

    @Override
    public Entry viewMyContestEntry(int userId, int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("from Entry as " +
                    "e join fetch e.contest as ec where ec.contestId=:id and e.user.userId=:userId", Entry.class);
            query.setParameter("id", id);
            query.setParameter("userId", userId);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Entry", id);
            return query.list().get(0);
        }
    }
    @Override
    public List<Review> viewMyContestEntryReviews(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review as " +
                    "r join fetch r.entry as re where re.entryId=:id", Review.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Entry", id);
            return query.list();
        }
    }

    @Override
    public void create(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entry);
        }
    }

    @Override
    public void update(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(entry);
            session.getTransaction().commit();
        }
    }
}
