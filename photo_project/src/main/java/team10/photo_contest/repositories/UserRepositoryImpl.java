package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.UserRepository;

import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.NO_ORGANIZERS_FOUND;
import static team10.photo_contest.exceptions.ErrorMessages.NO_USER_FOUND;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void deactivate(int id) {
        User userToDeactivate = getById(id);
        userToDeactivate.setActive(false);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userToDeactivate);
            session.getTransaction().commit();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public List<User> getAllAndSort() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User as u order by userScore.totalScore desc ", User.class);
            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(NO_USER_FOUND);
            }
            return result;
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where userName=:username", User.class);
            query.setParameter("username", username);
            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return result.get(0);
        }
    }

    @Override
    public List<User> getAllOrganizers() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from User as u join fetch u.userRoles as r " +
                    "where r.id=2", User.class);
            if (query.list().isEmpty()){
                throw new EntityNotFoundException(NO_ORGANIZERS_FOUND);
            }
            return query.list();
        }
    }

    @Override
    public boolean isUserMaster(String username) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from User as u join fetch u.userScore as s " +
                    "where u.userName=:username and s.totalScore>150", User.class);
            query.setParameter("username",username);
            return !query.list().isEmpty();
        }
    }
}
