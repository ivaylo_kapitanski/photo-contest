package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.ReviewsRepository;

import java.util.Date;
import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.NO_ENTRIES_FOUND;
import static team10.photo_contest.exceptions.ErrorMessages.NO_REVIEWS_FOUND;

@Repository
public class ReviewRepositoryImpl implements ReviewsRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Review getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Review review = session.get(Review.class, id);
            if (review == null) {
                throw new EntityNotFoundException("Review", id);
            }
            return review;
        }
    }

    @Override
    public List<Review> getAll() {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Review order by score desc ", Review.class);
            if(query.list().isEmpty()){
                throw new EntityNotFoundException("No reviews found.");
            }
            return query.list();
        }
    }


    @Override
    public List<Review> getByScore(int score) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Review " +
                    "where score=:score ", Review.class);
            query.setParameter("score",score);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_REVIEWS_FOUND);
            }
            return query.list();
        }
    }

    @Override
    public Review create(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(review);
            session.getTransaction().commit();
            return review;
        }
    }

    @Override
    public Review update(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(review);
            session.getTransaction().commit();
            return review;
        }
    }

    @Override
    public List<Review> getByEntry(int entryId) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Review " +
                    "where entry.entryId=:entryId ", Review.class);
            query.setParameter("entryId",entryId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_REVIEWS_FOUND);
            }
            return query.list();
        }
    }

    @Override
    public Review getByEntryAndJuror(int entryId,int userId) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Review r " +
                    "join fetch r.entry re where re.entryId=:entryId and r.createdBy=:userId", Review.class);
            query.setParameter("entryId",entryId);
            query.setParameter("userId",userId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_REVIEWS_FOUND);
            }
            return query.list().get(0);
        }
    }

    @Override
    public List<Entry> viewEntriesForInvitedContests(User user) {
        try (Session session = sessionFactory.openSession()) {
            var query = session.createQuery("from Entry as e join fetch e.contest as c " +
                    "where :user in elements(c.jury) and c.phaseITime < :date ", Entry.class);
            query.setParameter("user",user);
            query.setParameter("date",new Date());
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(NO_ENTRIES_FOUND);
            }
            return query.list();
        }
    }
}
