package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Category;
import team10.photo_contest.repositories.contracts.CategoryRepository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Category getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, id);
            if(category == null) throw new EntityNotFoundException("categories", id);
            return category;
        }
    }

    @Override
    public Category getByCategory(String category) {
        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where categoryType = :category", Category.class);
            query.setParameter("category", category);
            if(query.list().isEmpty()) throw new EntityNotFoundException("Category", "category", category);
            return query.list().get(0);
        }
    }

    @Override
    public List<Category> getAll() {

        try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category ", Category.class);
            if(query.list().isEmpty()) throw new EntityNotFoundException("Category");
            return query.list();
        }
    }
}
