package team10.photo_contest.exceptions;

public class ErrorMessages {

    public static final String NO_ENTRIES_IN_CONTEST = "No entries in contest";
    public static final String NO_CONTESTS_THIS_PHASE ="No contests in this phase found";
    public static final String NO_AVAILABLE_CONTESTS ="No available contests!";
    public static final String NO_OPEN_CONTESTS_THIS_PHASE ="No open contests in this phase found";
    public static final String NO_INVITATIONAL_CONTESTS_THIS_PHASE ="No invitational contests in this phase found";
    public static final String NO_OPEN_CONTESTS_FOUND ="No open contests found";
    public static final String NO_SUCH_CONTEST_FOR_USER ="No contest of this type for this user found";
    public static final String NO_REVIEWS_FOUND ="No jury reviews found";
    public static final String NO_ENTRIES_FOUND ="No entries found";
    public static final String NO_USER_FOUND ="User not found";
    public static final String NO_ORGANIZERS_FOUND ="Organizers not found";
    public static final String NO_ENTRIES_FROM_USER ="No entries for this user and contest found";
    public static final String CONTEST_IS_OPEN ="Contest is not invitational";
    public static final String CONTEST_TITLE_EXISTS ="Contest with this title already exists";
    public static final String NO_WINNING_ENTRIES ="No winning entries yet";
    public static final String USER_CAN_HAVE_ONE_ENTRY ="User can have only one entry per contest";
    public static final String ENTRY_VIEW_ERROR ="Only participants and organizers can view contest entries";
    public static final String PHOTO_EDIT_ERROR ="The entry photo cannot be edited";
    public static final String USER_ENTRY_NOT_ALLOWED ="Users can only add entries in Phase 1";
    public static final String USER_NOT_INVITED ="User is not invited";
    public static final String USER_IS_JURY ="User is jury for this contest";
    public static final String PHOTO_SAVE_EXCEPTION ="Photo could not be stored. Please check the URL or file and try again";
    public static final String PHOTO_NOT_FOUND ="Photo with id:%d not found";
    public static final String PHOTO_DUPLICATE ="Photo with this name already exists in your archive!";
    public static final String EXTENSION_NOT_CORRECT ="File extension not correct";
    public static final String DUPLICATE_USER ="User with this username already exists!";
    public static final String SCORE_CANNOT_BE_SHOWN ="Cannot show the score for this user.";
    public static final String SCORE_CANNOT_BE_UPDATED ="Cannot update the score for this user.";
    public static final String ACTION_FOR_ORGANIZERS ="This action is reserved for organizers!";
    public static final String PHOTO_MODIFY ="Users can modify only their own details or photos";
    public static final String REVIEW_MODIFY ="Jury members can modify only their own reviews.";
    public static final String COVER_PHOTO_EXISTS ="Cover photo in contest already exists";
    public static final String VALID_PHOTO_NAME ="Please input a valid file or file link and name!";
    public static final String PHOTO_MISSING ="Please provide contest photo!";




}
