package team10.photo_contest.exceptions;

public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found.", type, attribute, value));
    }

//    public EntityNotFoundException(String user) {
//        super(String.format("%s not found", user));
//    }
    public EntityNotFoundException(String message) {
        super(message);
    }
}
