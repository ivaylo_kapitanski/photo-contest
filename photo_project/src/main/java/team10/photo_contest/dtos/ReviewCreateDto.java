package team10.photo_contest.dtos;


import javax.validation.constraints.*;

public class ReviewCreateDto {

    @NotEmpty
    @Size(min = 2,message = "Comment must be longer than 2 symbols")
    private String comment;

    @Min(value=1, message = "Score must be grater than 0")
    @Max(value=10, message = "Score must not be greater than 10")
    private int score;

    @Positive
    private int entryId;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}
