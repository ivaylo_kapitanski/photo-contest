package team10.photo_contest.dtos;

import javax.validation.constraints.*;

public class ContestDto {
    @NotNull(message = "Title can not be null")
    @Size(min = 5, max = 50, message = "Title must be between 5 and 50 symbols!")
    private String title;

    @Size(min = 4, max = 5, message = "IsOpen can only be True or False")
    private String isOpen;

    //@NotEmpty(message = "PhaseITime can not be empty")
    @Min(value = 1, message = "PhaseITime min can only be 1 day")
    @Max(value = 31, message = "PhaseITime max can only be 31 day")
    private int phaseITime;

    //@NotEmpty(message = "PhaseIITime can not be empty")
    @Min(value = 1, message = "PhaseIITime min can only be 1 hour")
    @Max(value = 24, message = "PhaseIITime max can only be 24 hour")
    private int phaseIITime;

    @NotNull(message = "Category can not be null")
    private String category;

    private String photoUrl;

    private String photoName;

    private int photoIdFromUploaded;

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public int getPhaseITime() {
        return phaseITime;
    }

    public void setPhaseITime(int phaseITime) {
        this.phaseITime = phaseITime;
    }

    public int getPhaseIITime() {
        return phaseIITime;
    }

    public void setPhaseIITime(int phaseIITime) {
        this.phaseIITime = phaseIITime;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public int getPhotoIdFromUploaded() {
        return photoIdFromUploaded;
    }

    public void setPhotoIdFromUploaded(int photoIdFromUploaded) {
        this.photoIdFromUploaded = photoIdFromUploaded;
    }
}
