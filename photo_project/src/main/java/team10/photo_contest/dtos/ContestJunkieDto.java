package team10.photo_contest.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestJunkieDto {
    @NotNull(message = "Username can not be null")
    @Size(min = 2, max = 20, message = "Username length must be between 2 and 20 symbols")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
