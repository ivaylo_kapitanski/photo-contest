package team10.photo_contest.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class EntryDto {
    @Positive(message = "User id need to be larger than 0")
    private int userId;

    @Size(min = 2, max = 20, message = "Title size must be between 2 and 20")
    @NotNull(message = "Title can not be null")
    private String title;

    @Size(min = 20, max = 1024, message = "Story size must be between 20 and 1024")
    @NotNull(message = "Story can not be null")
    private String story;

    @Positive(message = "Contest id id need to be larger than 0")
    private int contestId;

    private String photoUrl;

    private String photoName;

    private int photoIdFromUploaded;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getPhotoIdFromUploaded() {
        return photoIdFromUploaded;
    }

    public void setPhotoIdFromUploaded(int photoIdFromUploaded) {
        this.photoIdFromUploaded = photoIdFromUploaded;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }
}
