package team10.photo_contest.dtos.output;

import java.util.List;

public class EntryWithReviewsDto {

    private int contestId;

    private int entryId;

    private String contestCategory;

    private String entryTitle;

    private String entryStory;

    private int createdBy;

    private int photoId;
    private String photoSoruce;

    private List<ReviewShowDto> reviewShowDto;

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(String contestCategory) {
        this.contestCategory = contestCategory;
    }

    public String getEntryTitle() {
        return entryTitle;
    }

    public void setEntryTitle(String entryTitle) {
        this.entryTitle = entryTitle;
    }

    public String getEntryStory() {
        return entryStory;
    }

    public void setEntryStory(String entryStory) {
        this.entryStory = entryStory;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public List<ReviewShowDto> getReviewShowDto() {
        return reviewShowDto;
    }

    public void setReviewShowDto(List<ReviewShowDto> reviewShowDto) {
        this.reviewShowDto = reviewShowDto;
    }

    public String getPhotoSoruce() {
        return photoSoruce;
    }

    public void setPhotoSoruce(String photoSoruce) {
        this.photoSoruce = photoSoruce;
    }
}
