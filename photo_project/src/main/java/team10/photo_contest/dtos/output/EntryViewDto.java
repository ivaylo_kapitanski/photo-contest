package team10.photo_contest.dtos.output;

public class EntryViewDto {
    private int contestId;

    private int entryId;

    private String contestCategory;

    private String entryTitle;

    private String entryStory;

    private int createdBy;

    private int photoId;
    private String photoSource;


    public String getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(String contestCategory) {
        this.contestCategory = contestCategory;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public int getEntryId() {
        return entryId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getEntryTitle() {
        return entryTitle;
    }

    public void setEntryTitle(String entryTitle) {
        this.entryTitle = entryTitle;
    }

    public String getEntryStory() {
        return entryStory;
    }

    public void setEntryStory(String entryStory) {
        this.entryStory = entryStory;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public String getPhotoSource() {
        return photoSource;
    }

    public void setPhotoSource(String photoSource) {
        this.photoSource = photoSource;
    }
}
