package team10.photo_contest.dtos.output;

import javax.validation.constraints.*;

public class ReviewShowDto {

    private String comment;

    private int score;

    private int reviewId;

    private int createdBy;

    @Positive
    private int entryId;

    public ReviewShowDto(String comment, int score, int reviewId, int createdBy, int entryId) {
        this.comment = comment;
        this.score = score;
        this.reviewId = reviewId;
        this.createdBy = createdBy;
        this.entryId = entryId;
    }

    public ReviewShowDto() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }
}
