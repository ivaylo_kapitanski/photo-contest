package team10.photo_contest.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class UserEditDto {

    @NotEmpty
    @Size(min = 2, max = 25,message = "First name must be between 2 and 25 symbols.")
    private String firstName;

    @NotEmpty
    @Size(min = 2, max = 25,message = "Last name must be between 2 and 25 symbols.")
    private String lastName;

    @NotEmpty
    @Size(min = 8, max = 35,message = "Password must be between 8 and 35 symbols.")
    private String password;

    @NotEmpty
    @Size(min = 2, max = 25,message = "Passwords must match!")
    private String repeatPassword;

    public UserEditDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}
