package team10.photo_contest.enums;

public enum Ranks {
    JUNKIE(0),
    ENTHUSIAST(51),
    MASTER(151),
    DICTATOR(1001);

    private int level;

    Ranks(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public Ranks getNext() {
        switch (this) {
            case JUNKIE:
                return ENTHUSIAST;

            case ENTHUSIAST:
                return MASTER;

            case MASTER:
                return DICTATOR;

            default:
                throw new IllegalStateException("Unexpected value: " + this);
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case JUNKIE:
                return "Junkie";

            case ENTHUSIAST:
                return "Enthusiast";

            case MASTER:
                return "Master";

            case DICTATOR:
                return "Wise and Benevolent Photo Dictator";

            default:
                throw new IllegalStateException("Unexpected value: " + this);
        }
    }
}
