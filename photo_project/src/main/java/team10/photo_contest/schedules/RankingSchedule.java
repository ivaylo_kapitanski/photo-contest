package team10.photo_contest.schedules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import team10.photo_contest.schedules.helper.RankingScheduleService;


@Component
public class RankingSchedule {
    private final RankingScheduleService rankingScheduleService;

    @Autowired
    public RankingSchedule(RankingScheduleService rankingScheduleService) {
        this.rankingScheduleService = rankingScheduleService;
    }

    @Scheduled(fixedRate = 60000, initialDelay = 1000)
    public void UpdateRankAndPhaseJob() {
        rankingScheduleService.rankJob();
    }
}
