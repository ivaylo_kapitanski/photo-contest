package team10.photo_contest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private int reviewId;

    @Column(name = "comment")
    private String comment;

    @Column(name = "score")
    private int score;

    @JsonIgnore
    @Column(name = "created_by")
    private int createdBy;

    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "entry_id")
    private Entry entry;

    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Entry getContestEntry() {
        return entry;
    }

    public void setContestEntry(Entry entry) {
        this.entry = entry;
    }
}
