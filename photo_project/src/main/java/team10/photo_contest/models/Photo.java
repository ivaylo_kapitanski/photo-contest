package team10.photo_contest.models;

import javax.persistence.*;

@Entity
@Table(name="photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="photo_id")
    private int photoId;

    @Column(name="source")
    private String source;

    @Column(name="user_id")
    private int userId;

    @Column(name="is_contest_photo")
    private boolean isContestPhoto;

    @Column(name="title")
    private String photoName;

    public Photo() {
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isContestPhoto() {
        return isContestPhoto;
    }

    public void setContestPhoto(boolean contestPhoto) {
        isContestPhoto = contestPhoto;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }
}
