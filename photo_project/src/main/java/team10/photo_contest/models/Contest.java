package team10.photo_contest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @Column(name = "is_open")
    private boolean isOpen;

    @Column(name = "phase_I_time")
    private Date phaseITime;

    @Column(name = "phase_II_time")
    private Date phaseIITime;

    @Column(name = "is_finished")
    private boolean isFinished;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cover_photo_id")
    private Photo photo;

    @JsonIgnore
    @ManyToMany
    @Fetch(FetchMode.JOIN)
    @JoinTable(name = "contests_jury",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> jury;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "invited_junkies",
            joinColumns = @JoinColumn(name = "contest_id", referencedColumnName = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    )
    private Set<User> invitedJunkies;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "contest_id")
    private Set<Entry> contestEntries;

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public Date getPhaseITime() {
        return phaseITime;
    }

    public void setPhaseITime(Date phaseITime) {
        this.phaseITime = phaseITime;
    }

    public Date getPhaseIITime() {
        return phaseIITime;
    }

    public void setPhaseIITime(Date phaseIITime) {
        this.phaseIITime = phaseIITime;
    }

    public Category getCategories() {
        return category;
    }

    public void setCategories(Category category) {
        this.category = category;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public Set<User> getJury() {
        return jury;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public Set<User> getInvitedJunkies() {
        return invitedJunkies;
    }

    public void setInvitedJunkies(Set<User> invitedJunkies) {
        this.invitedJunkies = invitedJunkies;
    }

    public Set<Entry> getContestEntries() {
        return contestEntries;
    }

    public void setContestEntries(Set<Entry> contestEntries) {
        this.contestEntries = contestEntries;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }
}
