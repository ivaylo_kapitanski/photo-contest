package team10.photo_contest.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.ReviewCreateDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.mapers.ReviewModelMapper;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.ReviewService;
import team10.photo_contest.services.contracts.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/organizer")
public class OrganizerMvcController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final ReviewModelMapper reviewModelMapper;
    private final ReviewService reviewService;

    @Autowired
    public OrganizerMvcController(UserService userService,
                                  AuthenticationHelper authenticationHelper,
                                  ContestService contestService,
                                  ReviewModelMapper reviewModelMapper,
                                  ReviewService reviewService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.reviewModelMapper = reviewModelMapper;
        this.reviewService = reviewService;
    }

    @GetMapping
    public String getPage(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            List<Contest> contests = new ArrayList<>();
            try{
                contests.addAll(contestService.viewInPhase1(user));
            } catch (EntityNotFoundException ignored) {}
            try{
                contests.addAll(contestService.viewInPhase2(user));
            } catch (EntityNotFoundException ignored) {}
            model.addAttribute("currentUser", user);
            model.addAttribute("openContests", contests);
            return "organizersHomePage";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/view/junkies/sorted")
    public String getJunkies(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            model.addAttribute("sortedJunkies", userService.getAllAndSort(user));
            return "organizerJunkies";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/contest/{contestId}/create/entry/{entryId}/review")
    public String getReviewDto(@PathVariable int contestId,
                               @PathVariable int entryId,
                               HttpSession session,
                               Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer() && user.getUserScore().getTotalScore() <= 150) return "redirect:/";
            model.addAttribute("contestId", contestId);
            model.addAttribute("entryId", entryId);
            model.addAttribute("reviewDto", new ReviewCreateDto());
            return "createReview";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/contest/{contestId}/create/entry/{entryId}/review")
    public String createReview(@PathVariable int contestId,
                               @PathVariable int entryId,
                               @Valid @ModelAttribute("reviewDto") ReviewCreateDto reviewCreateDto,
                               BindingResult errors,
                               HttpSession session,
                               Model model) {
        if(errors.hasErrors()) {
            return "createReview";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer() && user.getUserScore().getTotalScore() <= 150) return "redirect:/";
            reviewCreateDto.setEntryId(entryId);
            Review review = reviewModelMapper.fromDto(reviewCreateDto, user);
            reviewService.create(review, user);
            return String.format("redirect:/contests/%d/all-entries", contestId);
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", "error");
            return "createReview";
        }
    }

    @PostMapping("/contest/{contestId}/entry/{entryId}/category-not-correct")
    public String reviewCategoryAndEntryNotMatching(@PathVariable int contestId,
                                                    @PathVariable int entryId,
                                                    HttpSession session,
                                                    Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer() && user.getUserScore().getTotalScore() <= 150) return "redirect:/";
            Review review = reviewModelMapper.wrongCategory(entryId, user);
            reviewService.create(review, user);
            return String.format("redirect:/contests/%d/all-entries", contestId);
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        }  catch (DuplicateEntityException e) {
            model.addAttribute("error", "error");
            return "createReview";
        }
    }
}
