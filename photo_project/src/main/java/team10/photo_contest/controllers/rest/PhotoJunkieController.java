package team10.photo_contest.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.UserEditDto;
import team10.photo_contest.dtos.output.EntryWithReviewsDto;
import team10.photo_contest.dtos.output.RankDto;
import team10.photo_contest.dtos.UserRegisterDto;
import team10.photo_contest.mapers.UserModelMapper;
import team10.photo_contest.models.*;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.UserScoreService;
import team10.photo_contest.services.contracts.UserService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class PhotoJunkieController {
    private final UserModelMapper modelMapper;
    private final UserService userService;
    private final UserScoreService userScoreService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;

    @Autowired
    public PhotoJunkieController(UserModelMapper modelMapper,
                                 UserService userService,
                                 UserScoreService userScoreService,
                                 AuthenticationHelper authenticationHelper,
                                 ContestService contestService) {
        this.modelMapper = modelMapper;
        this.userService = userService;
        this.userScoreService = userScoreService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
    }

    @GetMapping
    @ApiOperation(value = "Find all available users in the system",
            notes = "Need authorization",
            response = User.class)
    public List<User> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getAllAndSort(user);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get user by id from system",
            notes = "Need authorization and id",
            response = User.class)
    public User getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return userService.getById(id, user);
    }

    @PostMapping
    @ApiOperation(value = "Create user from dto",
            notes = "Need authorization and dto",
            response = User.class)
    public User create(@Valid @RequestBody UserRegisterDto registerDto) {
        User user = modelMapper.fromDto(registerDto, new User());
        userService.create(user);
        userScoreService.create(user, new UserScore());
        return user;
    }

    @PutMapping("/{id}/deactivate")
    @ApiOperation(value = "Deactivate user by id",
            notes = "Need authorization and id")
    public void deactivate(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        userService.deactivate(id, user);
    }

    @PutMapping("/{id}/edit")
    @ApiOperation(value = "Edit user by id",
            notes = "Need authorization and id")
    public void editUser(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody UserEditDto dto) {
        User user = authenticationHelper.tryGetUser(headers);
        User userToUpdate = modelMapper.fromDto(dto, id);
        userService.update(userToUpdate, user);
    }

    @GetMapping("/contests/open")
    @ApiOperation(value = "User get open contests",
            notes = "Need authorization",
            response = Contest.class)
    public List<Contest> getOpenContestsInPhase1(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return contestService.viewOpenInPhase1();
    }
    @GetMapping("/{id}/contests/entered/finished")
    @ApiOperation(value = "Get user finished participating contests",
            notes = "Need authorization and user id",
            response = Contest.class)
    public List<Contest> getFinishedParticipatedContests(@RequestHeader HttpHeaders headers,
                                                         @Valid @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        return contestService.finishedContestForUser(id);
    }
    @GetMapping("/{id}/contests/entered/active")
    @ApiOperation(value = "Get user active entered contests",
            notes = "Need authorization and user id",
            response = Contest.class)
    public List<Contest> getActiveParticipatedContests(@RequestHeader HttpHeaders headers,
                                                         @Valid @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        return contestService.activeContestForUser(id);
    }

    @GetMapping("/contests/{id}/entry/reviews")
    @ApiOperation(value = "Get user finished participating contests",
            notes = "Need authorization and contest id",
            response = Contest.class)
    public List<Review> viewUserEntryFinishedContestReviews(@RequestHeader HttpHeaders headers,
                                                    @Valid @PathVariable int id) {
        User user=authenticationHelper.tryGetUser(headers);
        return contestService.viewFinishedContestEntryReviews(user, id);
    }

    @GetMapping("/contests/{id}/all-entries")
    @ApiOperation(value = "Get user reviews from finished participating contests",
            notes = "Need authorization and contest id",
            response = EntryWithReviewsDto.class)
    public List<EntryWithReviewsDto> viewUserAllEntryFinishedContestReviews(@RequestHeader HttpHeaders headers,
                                                                            @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewAllFinishedContestEntryReviews(user,id);
    }

    @GetMapping("/{id}/score")
    @ApiOperation(value = "Get user Score",
            notes = "Need authorization and contest id",
            response = RankDto.class)
    public RankDto getScore(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return userScoreService.getByUserId(id,user);
    }
}
