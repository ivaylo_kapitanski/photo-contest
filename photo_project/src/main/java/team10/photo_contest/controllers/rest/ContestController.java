package team10.photo_contest.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.*;
import team10.photo_contest.mapers.ContestModelMapper;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.PhotoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/contest")
public class ContestController {
    private final ContestService contestService;
    private final ContestModelMapper contestModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;

    @Autowired
    public ContestController(ContestService contestService,
                             ContestModelMapper contestModelMapper,
                             AuthenticationHelper authenticationHelper, PhotoService photoService) {
        this.contestService = contestService;
        this.contestModelMapper = contestModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
    }

    @GetMapping
    @ApiOperation(value = "Find all available contests in the system",
                  notes = "Need authorization",
                  response = Contest.class)
    public List<Contest> getAll(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.getAll(user);
    }

    @PostMapping
    @ApiOperation(value = "Create contest",
            notes = "Need contest dto and authorization",
            response = Contest.class)
    public Contest create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestDto contestDto) {
        User user = authenticationHelper.tryGetUser(headers);
        Contest contest = contestModelMapper.fromDto(contestDto);
        contestService.create(contest, user);
        return contest;
    }

    @GetMapping("/phase/1")
    @ApiOperation(value = "Find all available contests in phase 1",
            notes = "Need authorization",
            response = Contest.class)
    public List<Contest> viewInPhase1(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewInPhase1(user);
    }

    @GetMapping("/phase/2")
    @ApiOperation(value = "Find all available contests in phase 2",
            notes = "Need authorization",
            response = Contest.class)
    public List<Contest> viewInPhase2(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewInPhase2(user);
    }

    @GetMapping("/finished")
    @ApiOperation(value = "Find all available finished contests",
            notes = "Need authorization",
            response = Contest.class)
    public List<Contest> viewInFinished(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewFinished(user);
    }

    @GetMapping("/open")
    @ApiOperation(value = "Find all available open contests",
            notes = "Need authorization",
            response = Contest.class)
    public List<Contest> viewOpen(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewOpen(user);
    }

    @GetMapping("/{id}/junkies")
    @ApiOperation(value = "Find all junkies in contest",
            notes = "Need authorization and id for contest",
            response = Contest.class)
    public List<User> viewInJunkies(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewJunkies(id, user);
    }

    @GetMapping("/{id}/jury")
    @ApiOperation(value = "Find all jury in contest",
            notes = "Need authorization and id for contest",
            response = Contest.class)
    public List<User> viewInJury(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        return contestService.viewJury(id, user);
    }

    @PostMapping("/{id}/upload/photo")
    @ApiOperation(value = "Upload photo to contest",
            notes = "Need authorization, id for contest and photo file",
            response = Contest.class)
    public Contest addPhoto(@RequestHeader HttpHeaders headers,
                     @RequestParam("file") MultipartFile file,
                     @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Contest contest = contestModelMapper.addPhoto(photoService.storeFile(file, user), id);
        contestService.update(contest);
        return contest;
    }

    @PostMapping("/{id}/upload/photo/from-url")
    @ApiOperation(value = "Upload photo to contest",
            notes = "Need authorization, id for contest and photo url",
            response = Contest.class)
    public Contest addPhotoUrl(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhotoFromUrlDto photoFromUrlDto, @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Contest contest = contestModelMapper.addPhoto(
                photoService.storeFromUrl(photoFromUrlDto, user), id);
        contestService.update(contest);
        return contest;
    }

    @PostMapping("/{id}/add/junkies")
    @ApiOperation(value = "Add junkie to contest",
            notes = "Need authorization, id for contest and contest update dto",
            response = Contest.class)
    public Contest addJunkies(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestUpdateDto contestJunkies,
                       @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        List<String> junkiesToAdd = contestModelMapper.createList(contestJunkies);
        return contestService.updateJunkies(id, junkiesToAdd, user);
    }

    @PostMapping("/{id}/add/jury")
    @ApiOperation(value = "Add jury to contest",
            notes = "Need authorization, id for contest and contest update dto",
            response = Contest.class)
    public Contest addJury(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestUpdateDto contestJury,
                    @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        List<String> juryToAdd = contestModelMapper.createList(contestJury);
        return contestService.updateJury(id, juryToAdd, user);
    }
}
