package team10.photo_contest.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.ReviewCreateDto;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.mapers.ReviewModelMapper;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.ReviewService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/reviews")
public class ReviewController {
    private final AuthenticationHelper authenticationHelper;
    private final ReviewModelMapper modelMapper;
    private final ReviewService reviewService;

    @Autowired
    public ReviewController(AuthenticationHelper authenticationHelper,
                            ReviewModelMapper modelMapper,
                            ReviewService reviewService) {
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.reviewService = reviewService;
    }

    @PostMapping("/entries/{id}/category-not-correct")
    @ApiOperation(value = "Create review not correct category in entry",
            notes = "Need authorization and id",
            response = ReviewShowDto.class)
    public ReviewShowDto reviewCategoryAndEntryNotMatching(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Review review = modelMapper.wrongCategory(id, user);
        return reviewService.create(review, user);
    }

    @PostMapping
    @ApiOperation(value = "Create review in entry",
            notes = "Need authorization and ReviewCreateDto",
            response = ReviewShowDto.class)
    public ReviewShowDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ReviewCreateDto dto) {
        User user = authenticationHelper.tryGetUser(headers);
        Review review = modelMapper.fromDto(dto, user);
        return reviewService.create(review, user);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update review by id",
            notes = "Need authorization and id",
            response = ReviewShowDto.class)
    public ReviewShowDto updateReview(@RequestHeader HttpHeaders headers, @Valid @RequestBody ReviewCreateDto dto,
                                      @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Review review = modelMapper.fromDto(dto, user, id);
        return reviewService.update(review, user);
    }

    @GetMapping("/entries/user-is-jury")
    @ApiOperation(value = "Get entries where user i jury",
            notes = "Need authorization",
            response = ReviewShowDto.class)
    public List<EntryViewDto> viewEntriesWhereUserIsJury(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        return reviewService.viewEntriesForInvitedContests(user);
    }
}
