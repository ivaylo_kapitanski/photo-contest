package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.Helpers;
import team10.photo_contest.dtos.output.RankDto;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.models.UserScore;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.repositories.contracts.UserScoreRepository;

@ExtendWith(MockitoExtension.class)
public class UserScoreServiceImplTests {

    @Mock
    UserScoreRepository userScoreRepository;
    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserScoreServiceImpl userScoreService;


    @Test
    public void update_Should_ThrowException_When_UserNotFound() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();

        Mockito.when(userRepository.getById(mockUser.getUserId())).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userScoreService.update(mockUser, mockScore));
    }

    @Test
    public void update_Should_ThrowException_When_UserScoreNotFound() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();

        Mockito.when(userRepository.getById(mockUser.getUserId())).thenReturn(mockUser);
        Mockito.when(userScoreRepository.getById(mockScore.getId())).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userScoreService.update(mockUser, mockScore));
    }

    @Test
    public void update_Should_ThrowException_When_UserIDsDontMatch() {
        // Arrange
        var mockUser2 = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();
        mockUser2.setUserId(3);

        Mockito.when(userRepository.getById(mockUser2.getUserId())).thenReturn(mockUser2);
        Mockito.when(userScoreRepository.getById(mockScore.getId())).thenReturn(mockScore);


        //Act, Assert
        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> userScoreService.update(mockUser2, mockScore));
    }

    @Test
    public void update_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();

        Mockito.when(userRepository.getById(mockUser.getUserId())).thenReturn(mockUser);
        Mockito.when(userScoreRepository.getById(mockScore.getId())).thenReturn(mockScore);

        //Act
        userScoreService.update(mockUser, mockScore);

        //Assert
        Mockito.verify(userScoreRepository, Mockito.times(1))
                .update(Mockito.any(UserScore.class));
    }


    @Test
    public void create_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();

        //Act
        userScoreService.create(mockUser, mockScore);

        //Assert
        Mockito.verify(userScoreRepository, Mockito.times(1))
                .create(Mockito.any(UserScore.class));
    }

    @Test
    public void getById_Should_ThrowException_When_CallerChecksOwnScoreOrIsOrganizer () {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockCaller = Helpers.createMockUser1();
        mockCaller.setUserId(3);

        var mockScore = Helpers.createMockScore();
        mockScore.setUser(mockUser);
        mockUser.setUserScore(mockScore);
        Mockito.when(userScoreRepository.getByUserId(Mockito.anyInt())).thenReturn(mockScore);

        //Act, Assert
        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> userScoreService.getByUserId(mockUser.getUserId(), mockCaller));
    }
    @Test
    public void getById_Should_ShowResult_When_NothingIsThrown() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockCaller = Helpers.createMockUser2();

        var mockScore = Helpers.createMockScore();
        mockScore.setUser(mockUser);
        mockUser.setUserScore(mockScore);
        Mockito.when(userScoreRepository.getByUserId(Mockito.anyInt())).thenReturn(mockScore);

        //Act
        userScoreService.getByUserId(mockUser.getUserId(), mockCaller);

        //Assert
        Mockito.verify(userScoreRepository, Mockito.times(1))
                .getByUserId(mockUser.getUserId());
    }

    @Test
    public void getById_Should_ReturnCorrectRank_When_ScoreIsChecked() {
        //Arrange
        var mockUser = Helpers.createMockUser1();
        var mockCaller = Helpers.createMockUser2();

        var mockScore = Helpers.createMockScore();
        mockScore.setUser(mockUser);
        mockUser.setUserScore(mockScore);
        Mockito.when(userScoreRepository.getByUserId(Mockito.anyInt())).thenReturn(mockScore);
        userScoreService.getByUserId(mockUser.getUserId(), mockCaller);

        //Act
        RankDto result = userScoreService.getByUserId(mockUser.getUserId(), mockCaller);

        //Assert
        Assertions.assertEquals("Junkie", result.getCurrentRank());
        Assertions.assertEquals(0, result.getCurrentScore());
        Assertions.assertEquals("Enthusiast", result.getNextRank());
        Assertions.assertEquals(51, result.getNextScore());
    }
    @Test
    public void getById_Should_CallRepository() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        var mockScore=Helpers.createMockScore();

        mockUser.setUserScore(mockScore);

        //Act
        userScoreService.getById(Mockito.anyInt());

        // Assert
        Mockito.verify(userScoreRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getByRank_Should_CallRepository() {

        //Arrange, Act
        userScoreService.getByRank(Mockito.anyInt(),Mockito.anyInt());

        // Assert
        Mockito.verify(userScoreRepository, Mockito.times(1))
                .getByRank(Mockito.anyInt(),Mockito.anyInt());
    }
}
