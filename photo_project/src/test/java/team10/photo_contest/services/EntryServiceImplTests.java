package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.mapers.EntryModelMapper;
import team10.photo_contest.models.*;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.*;

import static team10.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class EntryServiceImplTests {
    @Mock
    EntryRepository entryRepository;

    @Mock
    VerificationHelper verificationHelper;

    @Mock
    EntryModelMapper modelMapper;

    @InjectMocks
    EntryServiceImpl entryService;

    @Test
    public void getById_Should_Return_EntryWithId() {
        //Arrange
        var entry = createMockEntry();

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(entry);

        //Act Assert
        Assertions.assertEquals(entryService.getById(0), entry);
        Mockito.verify(entryRepository, Mockito.times(1)).getById(0);
    }

    @Test
    public void getAll_Should_ReturnAll_Entries() {
        //Arrange
        var entry1 = createMockEntry();
        var entry2 = createMockEntry();
        entry2.setEntryId(2);
        entry2.setTitle("test");

        List<Entry> entries = new ArrayList<>();
        entries.add(entry1);
        entries.add(entry2);

        Mockito.when(entryRepository.getAll()).thenReturn(entries);

        //Act
        List<Entry> result = entryService.getAll();

        //Assert
        Assertions.assertTrue(result.contains(entry1));
        Assertions.assertTrue(result.contains(entry2));
    }

    @Test
    public void getByUserId_Should_Return_EntryWithUserId() {
        //Arrange
        var entry1 = createMockEntry();
        var entry2 = createMockEntry();
        entry2.setEntryId(2);
        entry2.setTitle("test");

        List<Entry> entries = new ArrayList<>();
        entries.add(entry1);
        entries.add(entry2);
        Mockito.when(entryRepository.getByUserId(Mockito.anyInt())).thenReturn(entries);

        //Act
        List<Entry> result = entryService.getByUserId(0);

        //Assert
        Assertions.assertTrue(result.contains(entry1));
        Assertions.assertTrue(result.contains(entry2));
        Mockito.verify(entryRepository, Mockito.times(1)).getByUserId(0);
    }

    @Test
    public void create_Should_createEntry() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        Set<Entry> entrySet = new HashSet<>();
        contest.setContestEntries(entrySet);
        entry.setContest(contest);

        //Act
        entryService.create(entry);
        //Assert
        Mockito.verify(entryRepository, Mockito.times(1)).create(entry);
    }

    @Test
    public void create_Should_ThrowWhenUserHaveEntry() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        entry.setContest(contest);

        //Act Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> entryService.create(entry));
        Mockito.verify(entryRepository, Mockito.times(0)).create(entry);
    }

    @Test
    public void create_Should_ThrowWhenContestIsClosed() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        contest.setInvitedJunkies(new HashSet<User>());
        contest.setOpen(false);

        Set<Entry> entrySet = new HashSet<>();
        contest.setContestEntries(entrySet);
        entry.setContest(contest);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> entryService.create(entry));
        Mockito.verify(entryRepository, Mockito.times(0)).create(entry);
    }

    @Test
    public void create_Should_ThrowWhenContestIsFinished() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        contest.setPhaseITime(new Date(1517092292));
        contest.setPhaseIITime(new Date(1517092292));
        Set<Entry> entrySet = new HashSet<>();
        contest.setContestEntries(entrySet);
        entry.setContest(contest);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> entryService.create(entry));
        Mockito.verify(entryRepository, Mockito.times(0)).create(entry);
    }

    @Test
    public void create_Should_Throw_WhenUserIsJuryInContest() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        var mockJury=createMockUser2();
        entry.setUser(mockJury);
        Set<Entry> entrySet = new HashSet<>();
        contest.setContestEntries(entrySet);
        contest.setJury(Set.of(mockJury));
        entry.setContest(contest);


        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> entryService.create(entry));
        Mockito.verify(entryRepository, Mockito.times(0)).create(entry);
    }

    @Test
    public void update_Should_UpdateEntry() {
        //Arrange
        var entry = createMockEntry();
        entry.setPhoto(null);
        //Act
        entryService.update(entry, createMockUser2());
        //Assert
        Mockito.verify(entryRepository, Mockito.times(1)).update(entry);
    }

    @Test
    public void update_Should_ShouldThrow() {
        //Arrange
        var entry = createMockEntry();
        entry.setPhoto(new Photo());

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> entryService.update(entry, createMockUser2()));
        Mockito.verify(entryRepository, Mockito.times(0)).update(entry);
    }

    @Test
    public void viewMyContestEntryReviews_Should_Throw_WhenUserIsNotInContest() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        var mockUser=createMockUser1();
        mockUser.setUserId(4);
        Set<Entry> entrySet = new HashSet<>();
        contest.setContestEntries(entrySet);
        entry.setContest(contest);
        Mockito.when(entryRepository.getById(Mockito.anyInt()))
                .thenReturn(entry);


        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> entryService.viewMyContestEntryReviews(Mockito.anyInt(),mockUser));
        Mockito.verify(entryRepository, Mockito.times(0)).viewMyContestEntryReviews(Mockito.anyInt());
    }

    @Test
    public void viewMyContestEntryReviews_Should_Get_WhenNothingIsThrown() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        var mockUser=createMockUser1();
        contest.setJury(Set.of(mockUser));
        entry.setUser(mockUser);
        entry.setContest(contest);
        contest.setContestEntries(Set.of(entry));
        Mockito.when(entryRepository.getById(Mockito.anyInt()))
                .thenReturn(entry);


        //Act
        entryService.viewMyContestEntryReviews(1,mockUser);
        //Assert
        Mockito.verify(entryRepository, Mockito.times(1))
                .viewMyContestEntryReviews(Mockito.anyInt());

    }

    @Test
    public void viewFinishedContestEntryScore_Should_Get_ReturnScoreWhenJuryIsPresent() {
        //Arrange
        var entry = createMockEntry();
        var contest = createMockContest();
        var mockUser=createMockUser1();
        var mockReview=createMockReview();
        mockReview.setScore(5);
        mockReview.setContestEntry(entry);
        entry.setReviews(Set.of(mockReview));
        contest.setJury(Set.of(mockUser));
        entry.setUser(mockUser);
        entry.setContest(contest);
        contest.setContestEntries(Set.of(entry));
        Mockito.when(entryRepository.getById(Mockito.anyInt()))
                .thenReturn(entry);


        //Act
        int result = entryService.viewFinishedContestEntryScore(1);

        //Assert
        Assertions.assertEquals(5, result);

    }

    @Test
    public void viewFinishedContestEntryScore_Should_Return_0_WhenNoReviews() {
        //Arrange
        var entry = createMockEntry();
        var contest = new Contest();
        var mockUser=createMockUser1();
        var mockReview=createMockReview();
        Set<User> entrySet = new HashSet<>();
        contest.setJury(entrySet);
        mockReview.setScore(5);
        mockReview.setContestEntry(entry);
        entry.setReviews(Set.of(mockReview));
        entry.setUser(mockUser);
        entry.setContest(contest);
        contest.setContestEntries(Set.of(entry));
        Mockito.when(entryRepository.getById(Mockito.anyInt()))
                .thenReturn(entry);


        //Act
        int result = entryService.viewFinishedContestEntryScore(1);

        //Assert
        Assertions.assertEquals(0, result);

    }
}
