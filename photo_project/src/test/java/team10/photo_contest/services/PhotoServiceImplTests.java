package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.Helpers;
import team10.photo_contest.controllers.utils.FileModel;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.exceptions.*;
import team10.photo_contest.models.Photo;
import team10.photo_contest.repositories.contracts.PhotoRepository;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceImplTests {
    @Mock
    PhotoRepository photoRepository;
    @Mock
    Path fileStorageLocation;

    @InjectMocks
    PhotoServiceImpl photoService;


    @Test
    public void storeFromUrl_Should_ThrowException_When_FileNameNotCorrect() {
        // Arrange
        PhotoFromUrlDto newPhoto = new PhotoFromUrlDto();
        newPhoto.setPhotoUrl("file..png");
        var mockCaller = Helpers.createMockUser1();

        //Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> photoService.storeFromUrl(newPhoto, mockCaller));
    }

    @Test
    public void storeFromUrl_Should_ThrowException_When_TimeLimitExceeded() throws IOException {
        // Arrange
        PhotoFromUrlDto newPhoto = new PhotoFromUrlDto();
        newPhoto.setPhotoUrl("file.png");
        var mockCaller = Helpers.createMockUser1();
        Path newPath = Path.of("SomeOtherLocation");

        when(fileStorageLocation.resolve(Mockito.anyString())).thenReturn(newPath);

        //Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> photoService.storeFromUrl(newPhoto, mockCaller));
    }

    @Test
    public void storeFromUrl_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        // Arrange
        PhotoFromUrlDto newPhoto = new PhotoFromUrlDto();
        newPhoto.setPhotoUrl("https://whiskystore.bg/userfiles/productlargeimages/product_2388.jpg");
        var mockCaller = Helpers.createMockUser1();
        Path newPath = Path.of("SomeOtherLocation");
        when(fileStorageLocation.resolve(Mockito.anyString())).thenReturn(newPath);

        //Act
        photoService.storeFromUrl(newPhoto, mockCaller);

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .create(Mockito.any(Photo.class));
    }

    @Test
    public void storeFile_Should_ThrowException_When_FileIsNotPhoto() {
        // Arrange
        MultipartFile file = new MockMultipartFile("File.excel", "content".getBytes());
        var mockCaller = Helpers.createMockUser1();

        //Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> photoService.storeFile(file, mockCaller));
    }

    @Test
    public void storeFile_Should_ThrowException_When_FileNameNotCorrect() {
        // Arrange
        MultipartFile file = new MockMultipartFile("....Fi..le.excel", "f..ile.jpg", "content", "content".getBytes());
        var mockCaller = Helpers.createMockUser1();

        //Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> photoService.storeFile(file, mockCaller));
    }

    @Test
    public void storeFile_Should_ThrowException_When_DuplicateFileNameExists() {
        // Arrange
        MultipartFile file = new MockMultipartFile("....Fi..le.excel", "f..ile.jpg", "content", "content".getBytes());
        var mockCaller = Helpers.createMockUser1();

        Mockito.when(photoRepository.checkIfPhotoNameIsUsed(Mockito.anyInt(), Mockito.anyString())).thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> photoService.storeFile(file, mockCaller));
    }

    @Test
    public void storeFileFromUrl_Should_ThrowException_When_DuplicateFileNameExists() {
        // Arrange
        PhotoFromUrlDto newPhoto = new PhotoFromUrlDto();
        newPhoto.setPhotoName("duplicatedName");
        newPhoto.setPhotoUrl("https://whiskystore.bg/userfiles/productlargeimages/product_2388.jpg");
        var mockCaller = Helpers.createMockUser1();

        Mockito.when(photoRepository.checkIfPhotoNameIsUsed(Mockito.anyInt(), Mockito.anyString())).thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> photoService.storeFromUrl(newPhoto, mockCaller));
    }

    @Test
    public void storeFile_Should_ThrowException_When_FileName() {
        // Arrange
        var mockCaller = Helpers.createMockUser1();
        MultipartFile file = new MockMultipartFile("....Fi..le.excel", "f..ile.jpg", "content", "content".getBytes());

        //Act, Assert
        Assertions.assertThrows(FileStorageException.class,
                () -> photoService.storeFile(file, mockCaller));
    }

    @Test
    public void storeFile_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        MultipartFile file = new MockMultipartFile("file.jpg", "file.jpg", "content", "content".getBytes());
        var mockCaller = Helpers.createMockUser1();
        Path newPath = Path.of("SomeOtherLocation");
        when(fileStorageLocation.resolve(Mockito.anyString())).thenReturn(newPath);
        //Act
        photoService.storeFile(file, mockCaller);

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .create(Mockito.any(Photo.class));
    }

    @Test
    public void viewPhoto_Should_ThrowException_When_PhotoNotFound() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        var mockCaller = Helpers.createMockUser1();
        //Act
        when(photoRepository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);
        //Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> photoService.viewPhoto(mockPhoto.getPhotoId(), mockCaller));
    }

    @Test
    public void viewPhoto_Should_ThrowException_When_CallerIsNotCreator() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        var mockCaller = Helpers.createMockUser1();
        mockPhoto.setUserId(3);
        //Act
        when(photoRepository.getById(Mockito.anyInt())).thenReturn(mockPhoto);
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.viewPhoto(mockPhoto.getPhotoId(), mockCaller));
    }

    @Test
    public void viewPhoto_Should_ThrowException_When_FileLocationNotFound() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        mockPhoto.setSource("file/location");
        var mockCaller = Helpers.createMockUser1();
        Path newPath = Path.of("ht://www.tutorialspoint.com/");
        //Act
        when(photoRepository.getById(Mockito.anyInt())).thenReturn(mockPhoto);
        when(fileStorageLocation.resolve(mockPhoto.getSource())).thenReturn(newPath);
        //Assert
        Assertions.assertThrows(MyFileNotFoundException.class,
                () -> photoService.viewPhoto(mockPhoto.getPhotoId(), mockCaller));
    }

    @Test
    public void update_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        //Act
        photoService.update(mockPhoto);

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .update(Mockito.any(Photo.class));
    }

    @Test
    public void delete_Should_ThrowException_When_PhotoIsInContest() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        mockPhoto.setContestPhoto(true);
        when(photoService.verifyIfContestPhoto(mockPhoto.getPhotoId())).thenReturn(true);
        //Act, Assert
        Assertions.assertThrows(UsedPhotoException.class,
                () -> photoService.delete(mockPhoto.getPhotoId()));
    }

    @Test
    public void delete_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        when(photoService.verifyIfContestPhoto(mockPhoto.getPhotoId())).thenReturn(false);
        //Act
        photoService.delete(mockPhoto.getPhotoId());

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .delete(Mockito.anyInt());
    }

    @Test
    public void getById_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        //Act
        photoService.getById(mockPhoto.getPhotoId());

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getByUserId_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockPhoto = Helpers.createMockPhoto();
        var mockPhoto2 = Helpers.createMockPhoto();
        var mockCaller = Helpers.createMockUser1();
        mockPhoto2.setPhotoId(2);
        List<Photo> list = new ArrayList<>();
        list.add(mockPhoto);
        list.add(mockPhoto2);
        when(photoRepository.getByUserId(mockCaller.getUserId())).thenReturn(list);
        //Act
        List<Photo> result = photoService.getByUserId(mockPhoto.getPhotoId());

        //Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .getByUserId(Mockito.anyInt());
        Assertions.assertEquals(1, result.get(0).getPhotoId());
        Assertions.assertEquals(2, result.get(1).getPhotoId());

    }

    @Test
    public void saveOrGetContestPhoto_Should_Throw_When_NothingIsEntered() {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        FileModel emptyFile = new FileModel();
        MockMultipartFile emptyMultipart = new MockMultipartFile(" ", new byte[0]);
        emptyFile.setFile(emptyMultipart);

        EntryDto entryDto = new EntryDto();
        entryDto.setPhotoUrl("");

        //Act, Assert
        Assertions.assertThrows(NotAllowedOperationException.class,
                () -> photoService.saveOrGetContestPhoto(emptyFile, entryDto, mockUser));
    }

    @Test
    public void saveOrGetContestPhoto_Should_SaveFromFile_And_Return_Photo() throws UnsupportedEncodingException {
        // Arrange
        var mockUser = Helpers.createMockUser1();
        FileModel emptyFile = new FileModel();
        MockMultipartFile file1 = new MockMultipartFile("myimage1.jpeg", "myimage1.jpeg", null, "Hello test one1111".getBytes("UTF-8"));
        emptyFile.setFile(file1);

        EntryDto entryDto = new EntryDto();
        entryDto.setPhotoUrl("");

        Path newPath = Path.of("SomeOtherLocation");

        when(fileStorageLocation.resolve(Mockito.anyString())).thenReturn(newPath);
        //Act
        Photo photo = photoService.saveOrGetContestPhoto(emptyFile, entryDto, mockUser);

        //Assert
        Assertions.assertEquals(emptyFile.getFile().getName(), photo.getPhotoName());
    }


}
