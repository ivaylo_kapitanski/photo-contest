package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.dtos.output.EntryWithReviewsDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.exceptions.ContestIsNotInvitationalException;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.models.*;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.*;

import static team10.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {
    @Mock
    ContestRepository contestRepository;

    @Mock
    VerificationHelper verificationHelper;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    ContestServiceImpl contestService;

    @Test
    public void getAll_Should_ReturnAll_Contests() {
        //Arrange
        var contests1 = createMockContest();
        var contests2 = createMockContest();
        contests2.setTitle("Dogs");

        List<Contest> contests = new ArrayList<>();
        contests.add(contests1);
        contests.add(contests2);

        Mockito.when(contestRepository.getAll()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.getAll(createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(contests1));
        Assertions.assertTrue(result.contains(contests2));
    }

    @Test
    public void getById_Should_ReturnContestById() {
        //Arrange
        var contest = createMockContest();

        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);

        //Act Assert
        Assertions.assertEquals(contestService.getById(1), contest);
    }

    @Test
    public void create_Should_ShouldThrow_WhenDuplicateExists() {
        //Arrange
        var contest1 = createMockContest();
        var contest2 = createMockContest();
        contest2.setContestId(0);

        Mockito.when(contestRepository.getByTitle(contest1.getTitle())).thenReturn(contest2);

        //Act Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> contestService.create(contest1, createMockUser2()));
    }

    @Test
    public void create_Should_ShouldThrow_WhenOrganizersNotSet() {
        //Arrange
        var contest1 = createMockContest();
        var contest2 = createMockContest();
        contest2.setContestId(1);
        contest2.setTitle("test");

        Mockito.when(contestRepository.getByTitle(contest1.getTitle())).thenReturn(contest2);
        Mockito.when(userRepository.getAllOrganizers()).thenThrow(EntityNotFoundException.class);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> contestService.create(contest1, createMockUser2()));
    }

    @Test
    public void create_IsCalledOnce() {
        //Arrange
        var contest1 = createMockContest();
        var contest2 = createMockContest();
        contest2.setContestId(1);
        contest2.setTitle("test");

        Mockito.when(contestRepository.getByTitle(contest1.getTitle())).thenReturn(contest2);

        //Act
        contestService.create(contest1, createMockUser2());

        //Assert
        Mockito.verify(contestRepository, Mockito.times(1)).create(contest1);
    }

    @Test
    public void getInPhase1_Should_ReturnInPhase1_Contests() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(2));
        contest.setPhaseIITime(calculateHours(calculateDays(2), 12));

        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.viewInPhase1()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.viewInPhase1(createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getInPhase2_Should_ReturnInPhase2_Contests() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(100), 12));

        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.viewInPhase2()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.viewInPhase2(createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getFinished_Should_ReturnFinished_Contests() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.viewFinished()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.viewFinished(createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getOpen_Should_ReturnOpen_Contests() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.viewOpen()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.viewOpen(createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getFinishedContestEntryReviews_Should_Throw_WhenEmptyEntries () {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        Set<Entry> entriesSet = new HashSet<>();
        contest.setContestEntries(entriesSet);

        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> contestService.viewFinishedContestEntryReviews(createMockUser1(), contest.getContestId()));
    }

    @Test
    public void getFinishedContestEntryReviews_Should_Return_WhenValid () {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        var review = createMockReview();

        Set<Entry> entriesSet = new HashSet<>();
        entriesSet.add(createMockEntry());

        List<Review> reviewList = new ArrayList<>();
        reviewList.add(review);

        contest.setContestEntries(entriesSet);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(contestRepository.viewFinishedContestEntryReviews(Mockito.anyInt(), Mockito.anyInt())).thenReturn(reviewList);

        //Act Assert
        Assertions.assertEquals(reviewList, contestService.viewFinishedContestEntryReviews(createMockUser1(), contest.getContestId()));
        Mockito.verify(contestRepository, Mockito.times(1)).viewFinishedContestEntryReviews(createMockUser1().getUserId(), contest.getContestId());
    }

    @Test
    public void getOpenInPhase1_Should_ReturnOpenInPhase_Contests() {
        //Arrange
        var contest = createMockContest();
        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.viewOpenInPhase1()).thenReturn(contests);

        //Act
        List<Contest> result = contestService.viewOpenInPhase1();

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getFinishedContestForUser_Should_ReturnFinishedContestForUser_Contests() {
        //Arrange
        var contest = createMockContest();
        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.finishedContestForUser(Mockito.anyInt())).thenReturn(contests);

        //Act
        List<Contest> result = contestService.finishedContestForUser(0);

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }

    @Test
    public void getActiveContestForUser_Should_ReturnActiveContestForUser_Contests() {
        //Arrange
        var contest = createMockContest();
        List<Contest> contests = new ArrayList<>();
        contests.add(contest);

        Mockito.when(contestRepository.activeContestForUser(Mockito.anyInt())).thenReturn(contests);

        //Act
        List<Contest> result = contestService.activeContestForUser(0);

        //Assert
        Assertions.assertTrue(result.contains(contest));
    }


    @Test
    public void viewJunkies_Should_ReturnJunkiesInContest() {
        //Arrange
        var user1 = createMockUser1();
        var user2 = createMockUser1();
        user2.setUserId(2);
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        Mockito.when(contestRepository.viewJunkies(Mockito.anyInt())).thenReturn(users);

        //Act
        List<User> result = contestService.viewJunkies(1, createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(user1));
        Assertions.assertTrue(result.contains(user2));
    }

    @Test
    public void viewJury_Should_ReturnJuryInContest() {
        //Arrange
        var user1 = createMockUser1();
        var user2 = createMockUser1();
        user2.setUserId(2);
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        Mockito.when(contestRepository.viewJury(Mockito.anyInt())).thenReturn(users);

        //Act
        List<User> result = contestService.viewJury(1, createMockUser2());

        //Assert
        Assertions.assertTrue(result.contains(user1));
        Assertions.assertTrue(result.contains(user2));
    }

    @Test
    public void getFinishedAllContestEntryReviews_Should_Throw_WhenEmptyEntries() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        Set<Entry> entriesSet = new HashSet<>();
        contest.setContestEntries(entriesSet);

        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> contestService.viewAllFinishedContestEntryReviews(createMockUser1(), contest.getContestId()));
    }

    @Test
    public void getFinishedAllContestEntryReviews_Should_Return_WhenValid() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        var entry = createMockEntry();
        entry.setContest(contest);
        Set<Review> reviewSet = new HashSet<>();
        reviewSet.add(createMockReview());
        entry.setReviews(reviewSet);

        List<Entry> EntryList = new ArrayList<>();
        EntryList.add(entry);

        List<EntryWithReviewsDto> reviewList = new ArrayList<>();
        EntryWithReviewsDto entryWithReviewsDto = new EntryWithReviewsDto();
        mapEntryToDto(entry, entryWithReviewsDto);
        reviewList.add(entryWithReviewsDto);

        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(contestRepository.viewAllFinishedContestEntryReviews(Mockito.anyInt())).thenReturn(EntryList);

        //Act Assert
        Assertions.assertEquals(reviewList.get(0).getContestId(), contestService.viewAllFinishedContestEntryReviews(createMockUser1(), contest.getContestId()).get(0).getContestId());
        Mockito.verify(contestRepository, Mockito.times(1)).viewAllFinishedContestEntryReviews(contest.getContestId());
    }

    @Test
    public void updateJury_Should_Return_WhenValid() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        Set<User> jury = new HashSet<>();
        contest.setJury(jury);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(userRepository.isUserMaster(Mockito.anyString())).thenReturn(true);

        //Act Assert
        Assertions.assertTrue(contestService.updateJury(0, users, createMockUser2()).getJury().contains(user));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJury_Should_ThrowWhenMaster() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        Set<User> jury = new HashSet<>();
        contest.setJury(jury);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJury(0, users, createMockUser2()));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJury_Should_ThrowWhenContestNotFound() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        Set<User> jury = new HashSet<>();
        contest.setJury(jury);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJury(0, users, createMockUser2()));
    }

    @Test
    public void updateJury_Should_ThrowWhenIsJuror() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        Set<User> jury = new HashSet<>();
        contest.setJury(jury);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(userRepository.isUserMaster(Mockito.anyString())).thenReturn(false);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJury(0, users, createMockUser2()));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJunkie_Should_Return_WhenValid() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        contest.setOpen(false);

        Set<User> junkies = new HashSet<>();
        contest.setInvitedJunkies(junkies);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(contestRepository.isJunkieInContest(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);
        Mockito.when(contestRepository.isJurorInContest(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);

        //Act Assert
        Assertions.assertTrue(contestService.updateJunkies(0, users, createMockUser2()).getInvitedJunkies().contains(user));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJunkie_Should_ThrowWhenJunkieInContest() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        contest.setOpen(false);

        Set<User> junkies = new HashSet<>();
        contest.setInvitedJunkies(junkies);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(contestRepository.isJunkieInContest(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);
        Mockito.when(contestRepository.isJurorInContest(Mockito.anyInt(), Mockito.anyInt())).thenReturn(false);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJunkies(0, users, createMockUser2()));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJunkie_Should_ThrowWhenContestNotFound() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        contest.setOpen(false);

        Set<User> junkies = new HashSet<>();
        contest.setInvitedJunkies(junkies);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenThrow(EntityNotFoundException.class);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJunkies(0, users, createMockUser2()));
    }

    @Test
    public void updateJunkie_Should_ThrowWhenIsJuror() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));
        contest.setOpen(false);

        Set<User> junkies = new HashSet<>();
        contest.setInvitedJunkies(junkies);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);
        Mockito.when(userRepository.getByUsername(Mockito.anyString())).thenReturn(user);
        Mockito.when(contestRepository.isJurorInContest(Mockito.anyInt(), Mockito.anyInt())).thenReturn(true);

        //Act Assert
        Assertions.assertThrows(NotAllowedOperationException.class, () -> contestService.updateJunkies(0, users, createMockUser2()));
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }

    @Test
    public void updateJunkie_Should_ThrowWhenContestISNotInvitational() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        Set<User> junkies = new HashSet<>();
        contest.setInvitedJunkies(junkies);

        var user = createMockUser1();

        List<String> users = new ArrayList<>();
        users.add(user.getUserName());

        Mockito.when(verificationHelper.checkIfContestExists(Mockito.anyInt())).thenReturn(contest);

        //Act Assert
        Assertions.assertThrows(ContestIsNotInvitationalException.class, () -> contestService.updateJunkies(0, users, createMockUser2()));
    }

    @Test
    public void update_Should_CallRepositoryOnce() {
        //Arrange
        var contest = createMockContest();
        contest.setPhaseITime(calculateDays(0));
        contest.setPhaseIITime(calculateHours(calculateDays(0), 0));

        contestService.update(contest);
        //Act Assert
        Mockito.verify(contestRepository, Mockito.times(1)).update(contest);
    }


    @Test
    public void contestsNotEnteredByUser_Should_NotThrow() {
        //Arrange
        List<Contest> contests = new ArrayList<>();
        List<Contest> empty = new ArrayList<>();
        contests.add(createMockContest());

        //Act Assert
        Assertions.assertEquals(contestService.contestsNotEnteredByUser(contests, createMockUser2()), empty);
    }

    @Test
    public void getEntriesByContestId_Should_NotThrow() {
        //Arrange
        List<Entry> entries = new ArrayList<>();
        entries.add(createMockEntry());

        Mockito.when(contestRepository.getEntriesByContestId(Mockito.anyInt())).thenReturn(entries);

        //Act Assert
        Assertions.assertEquals(contestService.getEntriesByContestId(1), entries);
    }

    @Test
    public void viewInvitationalInPhase1_Should_NotThrow() {
        //Arrange
        User user = createMockUser2();
        List<Contest> contests = new ArrayList<>();
        contests.add(createMockContest());
        Mockito.when(contestRepository.viewInvitationalInPhase1(user)).thenReturn(contests);

        //Act Assert
        Assertions.assertEquals(contestService.viewInvitationalInPhase1(user), contests);
    }

    /*@Test
    public void isContestPhase2_Should_returnTrue() {
        //Arrange
        Contest contest = createMockContest();
        contest.setPhaseITime(new Date().);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contests);

        //Act Assert
        Assertions.assertEquals(contestService.viewInvitationalInPhase1(user), contests);
    }*/

    private void mapEntryToDto(Entry entry, EntryWithReviewsDto dto) {
        dto.setContestId(entry.getContest().getContestId());
        dto.setEntryId(entry.getEntryId());
        dto.setEntryStory(entry.getStory());
        dto.setEntryTitle(entry.getTitle());
        dto.setCreatedBy(entry.getUser().getUserId());
        dto.setPhotoId(entry.getPhoto().getPhotoId());

        List<ReviewShowDto> tempList = new ArrayList<>();
        for (Review review : entry.getReviews()) {
            tempList.add(new ReviewShowDto(review.getComment(),
                    review.getScore(),
                    review.getReviewId(),
                    review.getCreatedBy(),
                    review.getContestEntry().getEntryId()));
        }
        dto.setReviewShowDto(tempList);
    }
}